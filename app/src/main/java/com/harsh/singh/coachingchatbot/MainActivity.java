package com.harsh.singh.coachingchatbot;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private FragmentManager mFragmentManager;
    private FragmentTransaction mFragmentTransaction;
    private FirstFragment firstFragment;
    private SecondFragment secondFragment;
    private int count=1;
    private Button Backbut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        Backbut = (Button) findViewById(R.id.BackBut);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mFragmentManager = getFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        firstFragment = new FirstFragment();
        secondFragment = new SecondFragment();
        mFragmentTransaction.add(R.id.RegFragments, firstFragment);
        mFragmentTransaction.commit();
    }

    public void NextButton(View view) {

        if(count==1) {

            mFragmentTransaction = mFragmentManager.beginTransaction();
            if (firstFragment.isResumed()) {
                mFragmentTransaction.replace(R.id.RegFragments, secondFragment);
                mFragmentTransaction.commit();
                count++;
            } else {
                mFragmentTransaction.replace(R.id.RegFragments, firstFragment);
            }

        }
        else if(count==2)
        {
            mFragmentTransaction = mFragmentManager.beginTransaction();
            if (secondFragment.isResumed()) {
                Intent intent = new Intent(MainActivity.this, ChatActivity.class);
                startActivity(intent);
            } else {
                mFragmentTransaction.replace(R.id.RegFragments, secondFragment);
            }
        }

    }

    public void BackButton(View view) {
        if(count==2)
        {
            mFragmentTransaction = mFragmentManager.beginTransaction();
            if (secondFragment.isResumed())
            {
                mFragmentTransaction.replace(R.id.RegFragments, firstFragment);
                mFragmentTransaction.commit();
                count--;
            } else
            {
                mFragmentTransaction.replace(R.id.RegFragments, secondFragment);
            }

        }
    }
}
