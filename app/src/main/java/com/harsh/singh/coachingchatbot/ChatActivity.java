package com.harsh.singh.coachingchatbot;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import ai.api.AIListener;
import ai.api.android.AIConfiguration;
import ai.api.android.AIService;
import ai.api.model.Result;

public class ChatActivity extends AppCompatActivity implements AIListener {

    private TextView resultTextView;
    private AIService aiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        resultTextView = (TextView) findViewById(R.id.resultTextView);

        if (ContextCompat.checkSelfPermission(ChatActivity.this,
                Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(ChatActivity.this,
                    new String[]{Manifest.permission.RECORD_AUDIO}, 4444);

        }

        final AIConfiguration config = new AIConfiguration("b99988d21ef34b66bfc5e9376a2b8fe0",
                AIConfiguration.SupportedLanguages.English,
                AIConfiguration.RecognitionEngine.System);
                aiService = AIService.getService(this, config);
                aiService.setListener(this);
    }

    public void listenButton(View view) {
        aiService.startListening();
    }


    @Override
    public void onResult(ai.api.model.AIResponse response) {
        Result result = response.getResult();
        final String speech = result.getFulfillment().getSpeech();
        resultTextView.setText("Bot : "+speech);

    }

    @Override
    public void onError(ai.api.model.AIError error) {
        resultTextView.setText(error.toString());
    }

    @Override
    public void onAudioLevel(float level) {
    }

    @Override
    public void onListeningStarted() {
    }

    @Override
    public void onListeningCanceled() {
    }

    @Override
    public void onListeningFinished() {
    }
}
